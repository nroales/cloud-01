package es.codeurjc.mca.practica_1_cloud_ordinaria_2021.image;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

@Service("storageService")
@Profile("production")
public class S3ImageService implements ImageService {

    @Value("${amazon.s3.bucket-name}")
    private String bucketName;

    private final AmazonS3 s3;

    public S3ImageService(AmazonS3 s3) {
        this.s3 = s3;
    }

    @Override
    public String createImage(MultipartFile multiPartFile) {
        String fileName;
        try {
            InputStream inputStream = multiPartFile.getInputStream();
            fileName = multiPartFile.getOriginalFilename();

            createS3Bucket();
            uploadFile(inputStream, fileName);
        } catch (IOException e) {
            throw new RuntimeException();
        }

        return s3.getUrl(bucketName, fileName).getPath();
    }

    @Override
    public void deleteImage(String image) {
        s3.deleteObject(bucketName, image);
    }

    private void createS3Bucket(){
        if(!s3.doesBucketExistV2(bucketName)) {
            s3.createBucket(bucketName);
        }
    }

    private void uploadFile(InputStream fileInputStream, String fileName){
        PutObjectRequest por = new PutObjectRequest(bucketName, fileName, fileInputStream, new ObjectMetadata());
        por.setCannedAcl(CannedAccessControlList.PublicRead);
        s3.putObject(por);
    }
    
}
